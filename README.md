<div style="text-align:center"><img style="object-fit:cover;"src="./assets/images/logo-dark.png" /></div>

<br/>
<br/>
<br/>

## iKline Mobile App

###### Application for scheduling medical appointments online.

B2C version of the project, the idea is to provide a centralized environment to customers of iKline's contracting organizations.

Initially, the application would be focused only on scheduling and managing appointments, enabling the integration and verification of clinic, physician and client calendars.

In this context, one can also include integrations with various agreements, further facilitating the search and scheduling process.

Finally, in the future, it opens up the possibility of adding as the main feature of the application, transforming it also into a data center, centralizing information from the various queries, in several places in a single place.

<br/>

## Getting Started

The next few steps shows how to install and setup your enviroment to run our mobile app.

---

##### 1. Open <code>.env.example</code> file, and copy it's content.

---

##### 2. Create a <code>.env</code> file, in the root directory of your project, and paste the copied content in it.

---

##### 3. Fill in the enviroment variables values as shown bellow.

```
API_BASE_URL=http://YOUR_IP:4000/v1
```

> NOTE: <code>YOUR_IP</code> must be replaced by your real ip since both the mobile app, and the backend server are running on different virtual machines.

---

##### 4. Install app dependencies.

```sh
> flutter pub get
```

---

##### 5. Generate auto code from JsonSerializable, Chopper and Mobx, to the communication with the API, and management of states.

```sh
> flutter pub run build_runner build --delete-conflicting-outputs
```

---

##### 6. Generate auto code from Intl, to the i18n and l10n of the app.

```sh
> flutter gen-l10n
```

---

##### 7. Start the app 🚀 .

```sh
> flutter pub run lib/main.dart
```

---
