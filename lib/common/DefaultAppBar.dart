import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class DefaultAppBar {
  static AppBar generate(BuildContext context,
      {String? title, List<Widget>? actions}) {
    return AppBar(
      centerTitle: true,
      foregroundColor: Theme.of(context).accentColor,
      backgroundColor: Theme.of(context).accentColor,
      title: Text(
        title ?? AppLocalizations.of(context)!.appTitle,
        style: Theme.of(context).textTheme.headline5,
      ),
      actions: actions,
    );
  }
}
