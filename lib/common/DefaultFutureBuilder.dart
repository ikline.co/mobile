import 'package:flutter/material.dart';

class DefaultFutureBuilder {
  static FutureBuilder<dynamic> generate(
    Future<dynamic> future,
    Widget Function(BuildContext) viewBuilder,
  ) {
    return FutureBuilder<dynamic>(
      future: future,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError) {
            return Container(
              child: Text(snapshot.error.toString()),
            );
          }
          return viewBuilder(context);
        } else {
          return Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation(Theme.of(context).accentColor),
            ),
          );
        }
      },
    );
  }
}
