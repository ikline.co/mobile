import 'package:flutter/material.dart';
import 'package:ikline_mobile/components/ShowDate.dart';

class AppointmentCard extends StatefulWidget {
  final name;
  final details;
  final date;

  AppointmentCard(this.name, this.details, this.date);

  @override
  _AppointmentCardState createState() => _AppointmentCardState();
}

class _AppointmentCardState extends State<AppointmentCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text(widget.name),
        subtitle: Text(widget.details),
        leading: Icon(Icons.local_hospital),
        trailing: ShowDate(widget.date),
      ),
    );
  }
}
