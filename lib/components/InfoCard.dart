import 'package:flutter/material.dart';

class InfoCard extends StatelessWidget {
  final title;
  final body;
  final setMargin;
  final setPadding;

  InfoCard(this.title, this.body, {this.setMargin, this.setPadding});

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: this.setMargin,
      child: DefaultTextStyle(
        style: Theme.of(context).textTheme.subtitle1!,
        child: Container(
          padding: this.setPadding,
          child: Text.rich(
            TextSpan(
              children: [
                    TextSpan(
                      text: this.title,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize:
                            DefaultTextStyle.of(context).style.fontSize! + 2,
                      ),
                    )
                  ] +
                  List.generate(body.length,
                      (index) => TextSpan(text: '\n' + body[index].join())),
            ),
          ),
        ),
      ),
    );
  }
}
