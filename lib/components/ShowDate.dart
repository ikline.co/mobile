import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ShowDate extends StatelessWidget {
  final DateTime date;

  ShowDate(this.date);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).accentColor,
        shape: BoxShape.rectangle,
        border: Border.all(
          color: Theme.of(context).accentColor,
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(5),
        ),
      ),
      padding: EdgeInsets.only(
        top: 10,
        bottom: 10,
        right: 13,
        left: 13,
      ),
      child: DefaultTextStyle(
        style: Theme.of(context).textTheme.bodyText1!.copyWith(
              color: Theme.of(context).primaryColor,
              fontWeight: FontWeight.bold,
            ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(DateFormat.MMM().format(date).toUpperCase()),
            Text(DateFormat.d().format(date)),
          ],
        ),
      ),
    );
  }
}
