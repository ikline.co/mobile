import 'package:flutter/material.dart';

class AppColors {
  final ThemeMode themeMode;

  AppColors(
    this.themeMode,
  );
  // Yet to implement:
  // static Color secondary =
  Color get primary => getColorFromTheme(Colors.white, Colors.grey[900]!);

  Color get accent => getColorFromTheme(Colors.blue, Colors.greenAccent[700]!);

  Color get success => getColorFromTheme(Colors.green, Colors.green);

  Color get warning => getColorFromTheme(Colors.yellow, Colors.yellow);

  Color get danger => getColorFromTheme(Colors.red, Colors.red);

  Color get white => getColorFromTheme(Colors.white70, Colors.white70);

  Color get black => getColorFromTheme(Colors.black87, Colors.black87);

  getColorFromTheme(Color lightColor, Color darkColor) {
    return themeMode == ThemeMode.light ? lightColor : darkColor;
  }
}
