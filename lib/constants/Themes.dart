import 'package:flutter/material.dart';
import 'package:ikline_mobile/constants/Colors.dart';

class AppThemes {
  late AppColors _appColors;

  AppThemes(ThemeMode themeMode) {
    _appColors = AppColors(themeMode);
  }

  ElevatedButtonThemeData get elevatedButtonTheme => ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          onPrimary: _appColors.white,
          primary: _appColors.accent,
        ),
      );

  TextButtonThemeData get textButtonTheme => TextButtonThemeData(
        style: TextButton.styleFrom(
          primary: _appColors.accent,
        ),
      );

  OutlinedButtonThemeData get outlinedButtonTheme => OutlinedButtonThemeData(
        style: OutlinedButton.styleFrom(
          primary: _appColors.accent,
        ),
      );

  BottomNavigationBarThemeData get bottomNavigationBarTheme =>
      BottomNavigationBarThemeData(
        backgroundColor: _appColors.primary,
        selectedItemColor: _appColors.accent,
        unselectedItemColor: _appColors.white,
      );

  static TextTheme textTheme = ThemeData.dark().textTheme;
}
