import 'dart:async';
import 'package:chopper/chopper.dart';
import 'package:ikline_mobile/models/Appointment.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

part 'WebService.chopper.dart';

@ChopperApi()
abstract class WebService extends ChopperService {
  static WebService create() {
    final client = ChopperClient(
      baseUrl: dotenv.env['API_BASE_URL'] ?? "localhost:4000/v1",
      services: [_$WebService()],
      converter: JsonConverter(),
      errorConverter: JsonConverter(),
    );
    return _$WebService(client);
  }

  @Get(path: "/appointments")
  Future<Response> getAppointments();

  @Post(path: "/appointments")
  Future<Response> postAppointment(@Body() Map appointment);

  @Delete(path: "/appointments/{id}")
  Future<Response> deleteAppointment(@Path("id") int id);

  @Get(path: "/companies")
  Future<Response> getCompanies();

  @Get(path: "/companies/{id}")
  Future<Response> getCompanyById(@Path("id") int id);
}
