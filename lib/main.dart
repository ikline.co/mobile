import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:ikline_mobile/http/WebService.dart';
import 'package:ikline_mobile/screens/NavigationBar.dart';
import 'package:ikline_mobile/constants/Colors.dart';
import 'package:ikline_mobile/constants/Fonts.dart';
import 'package:ikline_mobile/constants/Themes.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:ikline_mobile/stores/Stores.dart';

import 'package:flutter_dotenv/flutter_dotenv.dart';
Future main() async {
  await dotenv.load();
  runApp(IklineApp());
}

class IklineApp extends StatefulWidget {
  final profileStore = Stores.profileStore;
  @override
  _IklineAppState createState() => _IklineAppState();
}

class _IklineAppState extends State<IklineApp> {
  @override
  void initState() {
    super.initState();
    widget.profileStore.getThemeMode();
  }

  @override
  Widget build(BuildContext context) {
    return Provider(
        create: (_) => WebService.create(),
        dispose: (context, WebService service) => service.client.dispose(),
        child: Observer(
          builder: (BuildContext context) {
            final appColors = AppColors(widget.profileStore.themeMode);
            return MaterialApp(
              onGenerateTitle: (BuildContext context) =>
                  AppLocalizations.of(context)!.appTitle,
              localizationsDelegates: AppLocalizations.localizationsDelegates,
              supportedLocales: AppLocalizations.supportedLocales,
              theme: ThemeData(
                // Yet to implement.
                brightness: Brightness.light,
                // Colors
                primaryColor: appColors.primary,
                accentColor: appColors.accent,
                // Fonts
                fontFamily: AppFonts.main,
                // Themes
                textTheme: ThemeData.light().textTheme,
              ),
              darkTheme: ThemeData(
                brightness: Brightness.dark,
                // Colors
                primaryColor: appColors.primary,
                accentColor: appColors.accent,
                // Fonts
                fontFamily: AppFonts.main,
                // Themes
                textTheme: ThemeData.dark().textTheme,
                bottomNavigationBarTheme:
                    AppThemes(widget.profileStore.themeMode)
                        .bottomNavigationBarTheme,
                elevatedButtonTheme: AppThemes(widget.profileStore.themeMode)
                    .elevatedButtonTheme,
                textButtonTheme:
                    AppThemes(widget.profileStore.themeMode).textButtonTheme,
                outlinedButtonTheme: AppThemes(widget.profileStore.themeMode)
                    .outlinedButtonTheme,
              ),
              themeMode: widget.profileStore.themeMode,
              debugShowCheckedModeBanner: false,
              home: NavigationBar(),
            );
          },
        ));
  }
}
