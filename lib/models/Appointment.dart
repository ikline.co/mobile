import 'package:json_annotation/json_annotation.dart';
part 'Appointment.g.dart';

@JsonSerializable()
class Appointment {
  int? id;
  int companyId;
  DateTime date;

  Appointment({
    this.id,
    required this.companyId,
    required this.date,
  });

  factory Appointment.fromJson(Map<String, dynamic> json) =>
      _$AppointmentFromJson(json);
  Map<String, dynamic> toJson() => _$AppointmentToJson(this);
}
