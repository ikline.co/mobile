// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Appointment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Appointment _$AppointmentFromJson(Map<String, dynamic> json) {
  return Appointment(
    id: json['id'] as int?,
    companyId: json['companyId'] as int,
    date: DateTime.parse(json['date'] as String),
  );
}

Map<String, dynamic> _$AppointmentToJson(Appointment instance) =>
    <String, dynamic>{
      'id': instance.id,
      'companyId': instance.companyId,
      'date': instance.date.toIso8601String(),
    };
