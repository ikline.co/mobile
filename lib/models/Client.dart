import 'package:json_annotation/json_annotation.dart';
part 'Client.g.dart';

@JsonSerializable()
class Client {
  int id;
  String name;
  String email;

  Client({
    required this.id,
    required this.name,
    required this.email,
  });

  factory Client.fromJson(Map<String, dynamic> json) => _$ClientFromJson(json);
  Map<String, dynamic> toJson() => _$ClientToJson(this);
}
