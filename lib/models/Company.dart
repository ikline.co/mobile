import 'package:json_annotation/json_annotation.dart';
part 'Company.g.dart';

@JsonSerializable()
class Company {
  int id;
  String name;
  String details;
  String address;
  String picture;
  List<dynamic> openingHours;
  Object coordinates;

  Company({
    required this.id,
    required this.name,
    required this.details,
    required this.address,
    required this.picture,
    required this.openingHours,
    required this.coordinates,
  });

  factory Company.fromJson(Map<String, dynamic> json) =>
      _$CompanyFromJson(json);
  Map<String, dynamic> toJson() => _$CompanyToJson(this);
}
