// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Company.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Company _$CompanyFromJson(Map<String, dynamic> json) {
  return Company(
    id: json['id'] as int,
    name: json['name'] as String,
    details: json['details'] as String,
    address: json['address'] as String,
    picture: json['picture'] as String,
    openingHours: json['openingHours'] as List<dynamic>,
    coordinates: json['coordinates'] as Object,
  );
}

Map<String, dynamic> _$CompanyToJson(Company instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'details': instance.details,
      'address': instance.address,
      'picture': instance.picture,
      'openingHours': instance.openingHours,
      'coordinates': instance.coordinates,
    };
