import 'package:json_annotation/json_annotation.dart';
part 'ResponseList.g.dart';

@JsonSerializable()
class ResponseList {
  int? code;
  dynamic meta;
  List<dynamic>? data;

  ResponseList({required this.code, this.meta, this.data});

  factory ResponseList.fromJson(Map<String, dynamic> json) =>
      _$ResponseListFromJson(json);
  Map<String, dynamic> toJson() => _$ResponseListToJson(this);
}
