// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ResponseList.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseList _$ResponseListFromJson(Map<String, dynamic> json) {
  return ResponseList(
    code: json['code'] as int?,
    meta: json['meta'],
    data: json['data'] as List<dynamic>?,
  );
}

Map<String, dynamic> _$ResponseListToJson(ResponseList instance) =>
    <String, dynamic>{
      'code': instance.code,
      'meta': instance.meta,
      'data': instance.data,
    };
