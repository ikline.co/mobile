import 'package:json_annotation/json_annotation.dart';
part 'ResponseSingle.g.dart';

@JsonSerializable()
class ResponseSingle {
  int? code;
  dynamic meta;
  dynamic data;

  ResponseSingle({this.code, this.meta, required this.data});

  factory ResponseSingle.fromJson(Map<String, dynamic> json) =>
      _$ResponseSingleFromJson(json);
  Map<String, dynamic> toJson() => _$ResponseSingleToJson(this);
}
