// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ResponseSingle.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseSingle _$ResponseSingleFromJson(Map<String, dynamic> json) {
  return ResponseSingle(
    code: json['code'] as int?,
    meta: json['meta'],
    data: json['data'],
  );
}

Map<String, dynamic> _$ResponseSingleToJson(ResponseSingle instance) =>
    <String, dynamic>{
      'code': instance.code,
      'meta': instance.meta,
      'data': instance.data,
    };
