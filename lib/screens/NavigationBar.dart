import 'package:flutter/material.dart';
import 'package:ikline_mobile/screens/appointments/AppointmentList.dart';
import 'package:ikline_mobile/screens/home/Home.dart';
import 'package:ikline_mobile/screens/profile/Profile.dart';
import 'package:ikline_mobile/screens/schedule/ClinicList.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class NavigationBar extends StatefulWidget {
  final int inicialIndex;

  NavigationBar({this.inicialIndex = 0});

  @override
  _NavigationBarState createState() => _NavigationBarState();
}

class _NavigationBarState extends State<NavigationBar>
    with TickerProviderStateMixin {
  static List<Widget> _widgetOptions = <Widget>[
    Home(),
    AppointmentList(),
    ClinicList(),
    Profile(),
  ];
  int selectedIndex = 0;

  @override
  void initState() {
    super.initState();
    selectedIndex = widget.inicialIndex;
  }

  void _onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: AppLocalizations.of(context)!.homeNavBarLabel,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.comment),
            label: AppLocalizations.of(context)!.appointmentsNavBarLabel,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.calendar_today_outlined),
            label: AppLocalizations.of(context)!.scheduleNavBarLabel,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: AppLocalizations.of(context)!.profileNavBarLabel,
          ),
        ],
        currentIndex: selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}
