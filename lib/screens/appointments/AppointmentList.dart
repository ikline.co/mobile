import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:ikline_mobile/common/DefaultAppBar.dart';
import 'package:ikline_mobile/common/DefaultFutureBuilder.dart';
import 'package:ikline_mobile/stores/Stores.dart';
import 'package:ikline_mobile/components/AppointmentCard.dart';

class AppointmentList extends StatefulWidget {
  @override
  _AppointmentListState createState() => _AppointmentListState();
}

class _AppointmentListState extends State<AppointmentList> {
  final appointmentStore = Stores.appointmentStore;
  final companyStore = Stores.companyStore;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar.generate(context),
      body: DefaultFutureBuilder.generate(
        appointmentStore.fetchAppointments(context),
        this._buildListView,
      ),
    );
  }

  Widget _buildListView(BuildContext context) {
    return Observer(
        builder: (_) => ListView.builder(
              itemCount: appointmentStore.appointments.length,
              itemBuilder: (context, index) {
                final item = appointmentStore.appointments[index];

                final currentCompany = companyStore.companies
                    .firstWhere((element) => element.id == item.companyId);

                return Dismissible(
                  confirmDismiss: (DismissDirection direction) async {
                    switch (direction) {
                      case DismissDirection.endToStart:
                        return await appointmentStore
                            .deleteAppointment(context, item.id!)
                            .whenComplete(() {
                          setState(() {});
                        });
                      default:
                        //await editCard(context, item);
                        return false;
                    }
                  },
                  background: Container(
                    padding: EdgeInsets.symmetric(horizontal: 12.0),
                    color: Colors.grey,
                    alignment: Alignment.centerLeft,
                    child: Icon(Icons.edit),
                  ),
                  secondaryBackground: Container(
                    padding: EdgeInsets.symmetric(horizontal: 12.0),
                    color: Colors.red,
                    alignment: Alignment.centerRight,
                    child: Icon(Icons.delete),
                  ),
                  key: ValueKey(item),
                  onDismissed: (direction) {
                    setState(() {
                      appointmentStore.appointments.removeAt(index);
                    });
                  },
                  child: AppointmentCard(
                      currentCompany.name,
                      currentCompany.address,
                      appointmentStore.appointments[index].date),
                );
              },
            ));
  }

  // Future<bool> _deleteDialog(BuildContext context) async {
  //   return await showDialog(
  //     context: context,
  //     builder: (BuildContext context) {
  //       return AlertDialog(
  //         title: const Text("Confirm Cancellation"),
  //         content: const Text(
  //             "Are you sure you wish to delete and cancel this appointment?"),
  //         actions: <Widget>[
  //           TextButton(
  //               onPressed: () => Navigator.of(context).pop(true),
  //               child: const Text("Delete")),
  //           TextButton(
  //             onPressed: () => Navigator.of(context).pop(false),
  //             child: const Text("Cancel"),
  //           ),
  //         ],
  //       );
  //     },
  //   );
  // }
}
