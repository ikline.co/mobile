import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:ikline_mobile/common/DefaultAppBar.dart';
import 'package:ikline_mobile/screens/NavigationBar.dart';
import 'package:ikline_mobile/stores/Stores.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Home extends StatefulWidget {
  final profileStore = Stores.profileStore;
  final Image logoDark =
      Image.asset("assets/images/logo-dark.png", fit: BoxFit.fill);
  final Image logoLight =
      Image.asset("assets/images/logo-light.png", fit: BoxFit.fill);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    super.initState();
    widget.profileStore.getLocalImage();
    widget.profileStore.getUserData();
  }

  @override
  Widget build(BuildContext context) {
    bool lightMode = Theme.of(context).brightness == Brightness.light;
    return new Scaffold(
      appBar: DefaultAppBar.generate(
        context,
        actions: [
          IconButton(
            icon: lightMode
                ? Icon(Icons.dark_mode_outlined)
                : Icon(Icons.light_mode_outlined),
            onPressed: () => {
              widget.profileStore
                  .setThemeMode(lightMode ? ThemeMode.dark : ThemeMode.light)
            },
          )
        ],
      ),
      // backgroundColor: Theme.of(context).primaryColor,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _homepageHeader(context),
            Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: Theme.of(context).accentColor,
                  width: 3,
                ),
              ),
              child: lightMode ? widget.logoLight : widget.logoDark,
            ),
          ],
        ),
      ),
    );
  }

  Widget _homepageHeader(BuildContext context) {
    return Observer(builder: (BuildContext context) {
      final profileName = widget.profileStore.name;
      return profileName == ''
          ? GestureDetector(
              onTap: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (context) => NavigationBar(
                        inicialIndex: 3,
                      ),
                    ),
                    (Route<dynamic> route) => (false));
              },
              child: Padding(
                padding: const EdgeInsets.only(bottom: 50),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text.rich(TextSpan(
                      text: AppLocalizations.of(context)!.completeProfileData,
                      style: TextStyle(
                        fontSize:
                            DefaultTextStyle.of(context).style.fontSize! + 8,
                      ),
                    )),
                    Icon(Icons.edit),
                  ],
                ),
              ),
            )
          : Padding(
              padding: const EdgeInsets.only(bottom: 50),
              child: Text.rich(TextSpan(
                text: AppLocalizations.of(context)!.homeGreeting +
                    profileName.split(' ')[0] +
                    "!",
                style: TextStyle(
                  fontSize: DefaultTextStyle.of(context).style.fontSize! + 8,
                ),
              )),
            );
    });
  }
}
