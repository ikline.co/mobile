import 'package:flutter/material.dart';
import 'package:ikline_mobile/common/DefaultAppBar.dart';
import 'package:ikline_mobile/screens/NavigationBar.dart';
import 'package:ikline_mobile/stores/Stores.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Calendar extends StatefulWidget {
  final appointmentStore = Stores.appointmentStore;
  final companyId;

  Calendar(
    this.companyId,
  );

  @override
  _CalendarState createState() => _CalendarState();
}

class _CalendarState extends State<Calendar> {
  DateTime _focusedDay = DateTime.now();
  late DateTime _selectedDay;

  @override
  void initState() {
    super.initState();
    _selectedDay = _focusedDay;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar.generate(
        context,
        title: AppLocalizations.of(context)!.scheduleTitle,
      ),
      body: TableCalendar(
        firstDay: DateTime.utc(2010, 10, 16),
        lastDay: DateTime.utc(2030, 3, 14),
        focusedDay: _focusedDay,
        selectedDayPredicate: (day) => isSameDay(_selectedDay, day),
        onDaySelected: _onDaySelected,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          await widget.appointmentStore.postAppointment(context, {
            'companyId': widget.companyId,
            'date': DateFormat('yyyy/MM/dd').format(_selectedDay)
          });
          Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (context) => NavigationBar(
                      inicialIndex: 1,
                    ),
                  ),
                  (Route<dynamic> route) => (false))
              .whenComplete(() => setState(() {}));
        },
        child: Icon(Icons.add),
      ),
    );
  }

  _onDaySelected(DateTime selectedDay, DateTime focusedDay) {
    if (!isSameDay(_selectedDay, selectedDay)) {
      this.setState(() {
        _selectedDay = selectedDay;
        _focusedDay = focusedDay;
      });
    }
  }
}
