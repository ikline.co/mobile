import 'package:flutter/material.dart';
import 'package:ikline_mobile/common/DefaultAppBar.dart';
import 'package:ikline_mobile/components/InfoCard.dart';

import 'package:ikline_mobile/screens/schedule/Calendar.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:shimmer/shimmer.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ClinicDetail extends StatelessWidget {
  final String title;
  final openingHours;
  final String address;
  final String bodyText;
  final String image;
  final coordinates;
  final int companyId;
  static bool imageLoaded = false;

  ClinicDetail(this.title, this.bodyText, this.openingHours, this.image,
      this.address, this.coordinates, this.companyId);

  openMap(coordinates, title) async {
    final availableMaps = await MapLauncher.installedMaps;
    await availableMaps.first.showDirections(
        destination: Coords(coordinates['lat'], coordinates['lon']),
        destinationTitle: title);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar.generate(
        context,
        title: title,
        actions: [
          IconButton(
            icon: const Icon(Icons.map_outlined),
            onPressed: () => openMap(this.coordinates, this.title),
          )
        ],
      ),
      body: ListView(
        children: [
          Container(
            color: Colors.grey[700],
            margin: EdgeInsets.all(5.0),
            height: MediaQuery.of(context).size.height * 0.30,
            child: Image.network(
              image,
              loadingBuilder: (context, child, progress) {
                return progress == null
                    ? child
                    : Shimmer.fromColors(
                        child: Container(
                          color: Colors.grey[700],
                        ),
                        period: Duration(milliseconds: 1000),
                        baseColor: Colors.grey[700]!,
                        highlightColor: Theme.of(context).accentColor);
              },
              fit: BoxFit.fill,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Calendar(companyId),
                  ),
                );
              },
              child: Text(AppLocalizations.of(context)!.confirmSchedule),
            ),
          ),
          InfoCard(
            AppLocalizations.of(context)!.companyAddress,
            [
              [this.address]
            ],
            setPadding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
          ),
          InfoCard(
            AppLocalizations.of(context)!.companyOpeningHours,
            adjustOpeningHour(context, this.openingHours),
            setPadding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
          ),
          InfoCard(
            AppLocalizations.of(context)!.companyDescription,
            [
              [this.bodyText]
            ],
            setPadding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
          ),
        ],
      ),
    );
  }

  adjustOpeningHour(BuildContext context, openingHours) {
    return List.generate(openingHours.length, (index) {
      var hours = openingHours[index]['openingHours'];
      return ['${openingHours[index]['name']}:  '] +
          List.generate(openingHours[index]['openingHours'].length,
              (indexHour) {
            var close = hours[indexHour]['close'];
            var open = hours[indexHour]['open'];
            var combinedHours = (close != "")
                ? open + '-' + close
                : AppLocalizations.of(context)!.twentyFourHoursAttendance;
            return indexHour == 0 ? (combinedHours) : (' e ' + combinedHours);
          });
    });
  }
}
