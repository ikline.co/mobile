import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:ikline_mobile/common/DefaultAppBar.dart';
import 'package:ikline_mobile/common/DefaultFutureBuilder.dart';
import 'package:ikline_mobile/screens/schedule/ClinicDetail.dart';
import 'package:ikline_mobile/stores/Stores.dart';

class ClinicList extends StatefulWidget {
  final companyStore = Stores.companyStore;
  @override
  _ClinicListState createState() => _ClinicListState();
}

class _ClinicListState extends State<ClinicList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar.generate(context),
      body: Center(
        child: DefaultFutureBuilder.generate(
          widget.companyStore.fetchCompanies(context),
          this._buildListView,
        ),
      ),
    );
  }

  Widget _buildListView(BuildContext context) {
    return Observer(builder: (BuildContext context) {
      final companies = widget.companyStore.companies;
      return ListView.builder(
        itemBuilder: (context, index) {
          return Card(
            child: ListTile(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ClinicDetail(
                      companies[index].name,
                      companies[index].details,
                      companies[index].openingHours,
                      companies[index].picture,
                      companies[index].address,
                      companies[index].coordinates,
                      companies[index].id,
                    ),
                  ),
                );
              },
              leading: Icon(
                Icons.local_hospital,
                color: Theme.of(context).accentColor,
                size: 50,
              ),
              title: Padding(
                padding: const EdgeInsets.all(4.0),
                child: Text(
                  companies[index].name,
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.all(4.0),
                child: Text(companies[index].address),
              ),
            ),
          );
        },
        itemCount: companies.length,
      );
    });
  }
}
