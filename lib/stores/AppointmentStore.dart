import 'package:flutter/material.dart';
import 'package:ikline_mobile/models/ResponseList.dart';
import 'package:ikline_mobile/models/ResponseSingle.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';

import 'package:ikline_mobile/http/WebService.dart';
import 'package:ikline_mobile/models/Appointment.dart';

part 'AppointmentStore.g.dart';

class AppointmentStore = BaseAppointmentStore with _$AppointmentStore;

abstract class BaseAppointmentStore with Store {
  @observable
  List<Appointment> appointments = [];

  @action
  Future fetchAppointments(BuildContext context) async {
    final response = await Provider.of<WebService>(context).getAppointments();

    ResponseList responseList = ResponseList.fromJson(response.body);

    appointments = List<Appointment>.from(
      responseList.data!.map((data) => Appointment.fromJson(data)),
    );
  }

  @action
  Future postAppointment(BuildContext context, Map appointment) async {
    final response = await Provider.of<WebService>(context, listen: false)
        .postAppointment(appointment);

    ResponseSingle responseSingle = ResponseSingle.fromJson(response.body);

    appointments.add(Appointment.fromJson(responseSingle.data));
  }

  @action
  Future deleteAppointment(BuildContext context, int appointmentId) async {
    final response = await Provider.of<WebService>(context, listen: false)
        .deleteAppointment(appointmentId);

    ResponseSingle responseSingle = ResponseSingle.fromJson(response.body);

    if (responseSingle.data) {
      appointments.removeWhere((element) => element.id == appointmentId);
    }
  }
}
