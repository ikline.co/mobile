// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AppointmentStore.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AppointmentStore on BaseAppointmentStore, Store {
  final _$appointmentsAtom = Atom(name: 'BaseAppointmentStore.appointments');

  @override
  List<Appointment> get appointments {
    _$appointmentsAtom.reportRead();
    return super.appointments;
  }

  @override
  set appointments(List<Appointment> value) {
    _$appointmentsAtom.reportWrite(value, super.appointments, () {
      super.appointments = value;
    });
  }

  final _$fetchAppointmentsAsyncAction =
      AsyncAction('BaseAppointmentStore.fetchAppointments');

  @override
  Future<dynamic> fetchAppointments(BuildContext context) {
    return _$fetchAppointmentsAsyncAction
        .run(() => super.fetchAppointments(context));
  }

  final _$postAppointmentAsyncAction =
      AsyncAction('BaseAppointmentStore.postAppointment');

  @override
  Future<dynamic> postAppointment(
      BuildContext context, Map<dynamic, dynamic> appointment) {
    return _$postAppointmentAsyncAction
        .run(() => super.postAppointment(context, appointment));
  }

  final _$deleteAppointmentAsyncAction =
      AsyncAction('BaseAppointmentStore.deleteAppointment');

  @override
  Future<dynamic> deleteAppointment(BuildContext context, int appointmentId) {
    return _$deleteAppointmentAsyncAction
        .run(() => super.deleteAppointment(context, appointmentId));
  }

  @override
  String toString() {
    return '''
appointments: ${appointments}
    ''';
  }
}
