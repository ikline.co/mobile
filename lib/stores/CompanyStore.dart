import 'package:flutter/material.dart';
import 'package:ikline_mobile/models/ResponseList.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';

import 'package:ikline_mobile/http/WebService.dart';
import 'package:ikline_mobile/models/Company.dart';

part 'CompanyStore.g.dart';

class CompanyStore = BaseCompanyStore with _$CompanyStore;

abstract class BaseCompanyStore with Store {
  @observable
  List<Company> companies = [];

  @action
  Future fetchCompanies(BuildContext context) async {
    final response = await Provider.of<WebService>(context).getCompanies();

    ResponseList responseList = ResponseList.fromJson(response.body);
    companies = List<Company>.from(
      responseList.data!.map((data) => Company.fromJson(data)),
    );
  }
}
