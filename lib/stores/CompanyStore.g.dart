// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CompanyStore.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CompanyStore on BaseCompanyStore, Store {
  final _$companiesAtom = Atom(name: 'BaseCompanyStore.companies');

  @override
  List<Company> get companies {
    _$companiesAtom.reportRead();
    return super.companies;
  }

  @override
  set companies(List<Company> value) {
    _$companiesAtom.reportWrite(value, super.companies, () {
      super.companies = value;
    });
  }

  final _$fetchCompaniesAsyncAction =
      AsyncAction('BaseCompanyStore.fetchCompanies');

  @override
  Future<dynamic> fetchCompanies(BuildContext context) {
    return _$fetchCompaniesAsyncAction.run(() => super.fetchCompanies(context));
  }

  @override
  String toString() {
    return '''
companies: ${companies}
    ''';
  }
}
