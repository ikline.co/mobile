import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'ProfileStore.g.dart';

class ProfileStore = BaseProfileStore with _$ProfileStore;

abstract class BaseProfileStore with Store {
  @observable
  ThemeMode themeMode = ThemeMode.dark;

  @observable
  String profilePicture = '';

  @observable
  String name = '';

  @observable
  String email = '';

  @observable
  String phoneNumber = '';

  @observable
  String pinCode = '';

  @observable
  String locationState = '';

  @action
  Future getLocalImage() async {
    final prefs = await SharedPreferences.getInstance();
    final localPicture = prefs.getString('profilePicture');
    if (localPicture != null) {
      profilePicture = localPicture;
    }
  }

  @action
  Future setLocaImage(imageString) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('profilePicture', imageString);
    profilePicture = imageString;
  }

  @action
  Future saveUserData(userData) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('name', userData['name']);
    name = userData['name'];

    prefs.setString('email', userData['email']);
    email = userData['email'];

    prefs.setString('phoneNumber', userData['phoneNumber']);
    phoneNumber = userData['phoneNumber'];

    prefs.setString('pinCode', userData['pinCode']);
    pinCode = userData['pinCode'];

    prefs.setString('locationState', userData['locationState']);
    locationState = userData['locationState'];
  }

  @action
  Future getUserData() async {
    final prefs = await SharedPreferences.getInstance();
    name = prefs.getString('name') ?? '';

    email = prefs.getString('email') ?? '';

    phoneNumber = prefs.getString('phoneNumber') ?? '';

    pinCode = prefs.getString('pinCode') ?? '';

    locationState = prefs.getString('locationState') ?? '';
  }

  @action
  Future getThemeMode() async {
    final prefs = await SharedPreferences.getInstance();
    final darkMode = prefs.getBool('darkMode');
    if (darkMode != null) {
      themeMode = darkMode ? ThemeMode.dark : ThemeMode.light;
    }
  }

  @action
  Future setThemeMode(ThemeMode mode) async {
    final prefs = await SharedPreferences.getInstance();
    themeMode = mode;
    final darkMode = mode == ThemeMode.dark ? true : false;
    prefs.setBool('themeMode', darkMode);
  }
}
