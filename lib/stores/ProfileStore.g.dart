// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ProfileStore.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ProfileStore on BaseProfileStore, Store {
  final _$themeModeAtom = Atom(name: 'BaseProfileStore.themeMode');

  @override
  ThemeMode get themeMode {
    _$themeModeAtom.reportRead();
    return super.themeMode;
  }

  @override
  set themeMode(ThemeMode value) {
    _$themeModeAtom.reportWrite(value, super.themeMode, () {
      super.themeMode = value;
    });
  }

  final _$profilePictureAtom = Atom(name: 'BaseProfileStore.profilePicture');

  @override
  String get profilePicture {
    _$profilePictureAtom.reportRead();
    return super.profilePicture;
  }

  @override
  set profilePicture(String value) {
    _$profilePictureAtom.reportWrite(value, super.profilePicture, () {
      super.profilePicture = value;
    });
  }

  final _$nameAtom = Atom(name: 'BaseProfileStore.name');

  @override
  String get name {
    _$nameAtom.reportRead();
    return super.name;
  }

  @override
  set name(String value) {
    _$nameAtom.reportWrite(value, super.name, () {
      super.name = value;
    });
  }

  final _$emailAtom = Atom(name: 'BaseProfileStore.email');

  @override
  String get email {
    _$emailAtom.reportRead();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.reportWrite(value, super.email, () {
      super.email = value;
    });
  }

  final _$phoneNumberAtom = Atom(name: 'BaseProfileStore.phoneNumber');

  @override
  String get phoneNumber {
    _$phoneNumberAtom.reportRead();
    return super.phoneNumber;
  }

  @override
  set phoneNumber(String value) {
    _$phoneNumberAtom.reportWrite(value, super.phoneNumber, () {
      super.phoneNumber = value;
    });
  }

  final _$pinCodeAtom = Atom(name: 'BaseProfileStore.pinCode');

  @override
  String get pinCode {
    _$pinCodeAtom.reportRead();
    return super.pinCode;
  }

  @override
  set pinCode(String value) {
    _$pinCodeAtom.reportWrite(value, super.pinCode, () {
      super.pinCode = value;
    });
  }

  final _$locationStateAtom = Atom(name: 'BaseProfileStore.locationState');

  @override
  String get locationState {
    _$locationStateAtom.reportRead();
    return super.locationState;
  }

  @override
  set locationState(String value) {
    _$locationStateAtom.reportWrite(value, super.locationState, () {
      super.locationState = value;
    });
  }

  final _$getLocalImageAsyncAction =
      AsyncAction('BaseProfileStore.getLocalImage');

  @override
  Future<dynamic> getLocalImage() {
    return _$getLocalImageAsyncAction.run(() => super.getLocalImage());
  }

  final _$setLocaImageAsyncAction =
      AsyncAction('BaseProfileStore.setLocaImage');

  @override
  Future<dynamic> setLocaImage(dynamic imageString) {
    return _$setLocaImageAsyncAction.run(() => super.setLocaImage(imageString));
  }

  final _$saveUserDataAsyncAction =
      AsyncAction('BaseProfileStore.saveUserData');

  @override
  Future<dynamic> saveUserData(dynamic userData) {
    return _$saveUserDataAsyncAction.run(() => super.saveUserData(userData));
  }

  final _$getUserDataAsyncAction = AsyncAction('BaseProfileStore.getUserData');

  @override
  Future<dynamic> getUserData() {
    return _$getUserDataAsyncAction.run(() => super.getUserData());
  }

  final _$getThemeModeAsyncAction =
      AsyncAction('BaseProfileStore.getThemeMode');

  @override
  Future<dynamic> getThemeMode() {
    return _$getThemeModeAsyncAction.run(() => super.getThemeMode());
  }

  final _$setThemeModeAsyncAction =
      AsyncAction('BaseProfileStore.setThemeMode');

  @override
  Future<dynamic> setThemeMode(ThemeMode mode) {
    return _$setThemeModeAsyncAction.run(() => super.setThemeMode(mode));
  }

  @override
  String toString() {
    return '''
themeMode: ${themeMode},
profilePicture: ${profilePicture},
name: ${name},
email: ${email},
phoneNumber: ${phoneNumber},
pinCode: ${pinCode},
locationState: ${locationState}
    ''';
  }
}
