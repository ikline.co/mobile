import 'CompanyStore.dart';
import 'AppointmentStore.dart';
import 'ProfileStore.dart';

class Stores {
  static ProfileStore profileStore = ProfileStore();
  static AppointmentStore appointmentStore = AppointmentStore();
  static CompanyStore companyStore = CompanyStore();
}
